# Online-tau-ID

The "Online-tau-ID" repo aims to help with data prepration, visualisation and the traning process of your favourite ML algorithm. 

## Required Data Format

All the available xAOD samples are updated in the samples.txt   
- xAOD to MxAOD : This step is done using [THOR](https://gitlab.cern.ch/atlas-perf-tau/THOR)
- MxAOD to NTuples : This is done using [NTUPLER](https://gitlab.cern.ch/soerdek/ntupler/)

With the Ntuples ready, we are good to go to train our Tau ID algorithm

# Getting started with processing samples

To get started with the "Online-tau-ID" project, follow these steps:

## Clone the repository to your local machine:

```bash
git clone ssh://git@gitlab.cern.ch:7999/asudhaka/online-tau-id.git
```

## Convert Ntuples into HDF5 files:
    
   - In preparing samples use ntup_to_h5.py to convert Ntuples to H5s
   - Change the ntup_to_h5_config.yaml file as you need. Important things to change:
	   - tree name 
	   - input_folder
	   - output_name
	   - n_jets: change depending on the sample
	   - max_tracks, max_cells depending on the need

Usage:
```bash
python ntup_to_h5.py --config ntup_to_h5_config.yaml
```

## Truth Labelling and prong splits

- Use the cuts_and_prongs.py script to make all the baseline cuts and split the dataset based on prongness, signal and background
- This script also creates a new jet lable called `HadronConeExclTruthLabelID` which is 5 for taus and 0 for qcd 
	- The 5 is just a placeholder for b jets since this framework is used for b tagging
- Important things to change :
    - the paths of the input/output dataset
- Any additional baseline cuts can be added as a mask in the split dataset function 

Usgae:
```shell
python cuts_and_prongs.py -p [0,1,m] -s ['Signal','Background']
```

## Sample proportion modifier (Optional)

To mix samples from different processes with different proportions, this step might be useful since this this very difficult ot be performed downstream. 
- Firstly genrate a yaml file which contains the jets to get from each sample using this notebook `Preparing_samples/umami/config/sample_prop_calc.ipynb`. 
- Important things to change:
    - Version number
    - processes
    - sample weights
    - path to the directory where the samples from the previous step is stored
- Running this will generate a config file with the required jets 

This config file is used to get the reweighted samples using the script `/Preparing_samples/reweight_samples.py`

Usgae:
```shell
python reweight_samples.py --config path/to/config.yaml 
```

## Test splits (Optional)

To make test splits with different process combinations (For example Gammatautau signal and ttbar background) one can use the `split_test.py` and `merge_test.py` scripts. 

# Umami reweighting

Used to split train/val/test datasets and to reweight train samples 
- Create an environment, enter it and install umami:
```shell
python3 -m venv umami
source umami/bin/activate
python -m pip install umami-preprocessing
```

- Check out [UMAMI docs](https://umami-hep.github.io/umami-preprocessing/sampling/) to explore various configurations
- Change the train/val/test jet counts (If previous step is peprformed, look at the generated config file for the approximate available jet counts from signal and background)
```shell
preprocess --config tau.yaml --split all
```
- Arrange all the output files as they are not the desired folder

# SALT training
- Create an new environment, enter it and install salt:
```shell
python3 -m venv salt
source salt/bin/activate
python -m pip install salt-ml
```
For salt documentaion refer [here](https://ftag-salt.docs.cern.ch/).

- Create a [comet account](https://www.comet.com/) to log and monitor all the training easily. Create a workspace and project and copy the api key. Paste it in the config file. And change the project name as well 
	- To make it work, you might have to add these to the bashrc file: `export COMET_API_KEY=""` and `export COMET_WORKSPACE=""`
- Important thigns to change:
    - variables 
    - hyper-parameters
    -  `num_workers`
    - `batch_size` 
    - accelerator 
    - `train_file`
    - `val_file`
    - `test_file`
    - `class_dict`
    - `norm_dict`
    - `num_inputs` tracks and cells to use for training
- and if comet is being used:
    - `api_key`
    - `project_name`
    - `workspace`

- Once the config is setup, to start the training:
```shell
salt fit --config path/to/config.yaml --force
```

- Monitor the progress using comet:
- Test the model using the salt generated config, which is stored in the workspace path
```bash 
salt test --config path/to/salt_generated_config.yaml
```
- In case there are mmultiple test files, use `SALT_training/salt_test.sh` with modified paths and arguments



# Getting started with plotter

To plot the signal and background distributions of your desired variable, use the `plotter.py` script. This utilizes multiprocessing to load data from multiple files concurrently. You can customize plot properties such as figure size, DPI, histogram type, axis labels, and more, and also perform specified operations on data before plotting. 


In the config file, you can:
- Change the path of the input H5 files 
- Change the output path
- Add variables you want to plot
- Save in pdf or png format


Once the config file is setup, you are ready to plot the variables.

```shell
python plotter.py -c plotter_config.py -t jets / tracks / cells
```
