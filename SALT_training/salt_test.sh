#!/bin/bash

PATH_ARG="1p"
CONFIG_PATH="/home/aponnu/Desktop/AthulFiles/salt_outputs/1p/GNTau_SC5_HP0_20241125-T215303/config.yaml"

source AthulFiles/salt/bin/activate

salt test --config $CONFIG_PATH --data.test_file "/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/${PATH_ARG}/gammatautau(Sig)-dijet(Bg).h5" --force
salt test --config $CONFIG_PATH --data.test_file "/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/${PATH_ARG}/gammatautau(Sig)-ttbar(Bg).h5" --force
salt test --config $CONFIG_PATH --data.test_file "/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/${PATH_ARG}/ttbar(Sig)-dijet(Bg).h5" --force
salt test --config $CONFIG_PATH --data.test_file "/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/${PATH_ARG}/ttbar(Sig)-ttbar(Bg).h5" --force
salt test --config $CONFIG_PATH --data.test_file "/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/${PATH_ARG}/Ztautau(Sig)-dijet(Bg).h5" --force
salt test --config $CONFIG_PATH --data.test_file "/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/${PATH_ARG}/Ztautau(Sig)-ttbar(Bg).h5" --force