import h5py,os
import numpy as np
from puma import Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import logger
from puma import IntegratedEfficiency, IntegratedEfficiencyPlot
from puma import VarVsEff, VarVsEffPlot
import matplotlib.pyplot as plt
import matplotlib as mpl
import atlas_mpl_style as ampl
ampl.use_atlas_style()

def get_networks(prong,models,sample,debug = False):
    base_path = f'/home/aponnu/Desktop/AthulFiles/salt_outputs/{prong}'
    networks = {}
    for model in models:
        if model == 'Deepset':
            networks['Deepset'] = networks[models[0]]
            continue  
        for folder in os.listdir(base_path):
            if model not in folder:
                continue
            for file in os.listdir(f'{base_path}/{folder}/ckpts'):
                if sample in file:
                    networks[model] = f'{base_path}/{folder}/ckpts/{file}'
    return networks


def disc_fct(arr: np.ndarray) -> np.ndarray:
    # return np.log((arr[0] + 1e-8) / (arr[1] + 1e-8))
    # return -np.log((1 - arr[0] + 1e-8))
    return arr[0]

def get_info(key,val,prong,pT_bound=[20,500]):
    f = h5py.File(val, 'r')
    if prong == '0p':
        eff = np.linspace(0.45, 1, 101)
    if prong == '1p':
        eff = np.linspace(0.93, 1, 101)
    elif prong == 'mp':
        eff =  np.linspace(0.7, 1, 121)

    main_mask = (f['jets']['TauJets.pt'] / 1000 > pT_bound[0]) & (f['jets']['TauJets.pt'] / 1000 < pT_bound[1])
    pt = f['jets']['TauJets.pt'][main_mask] / 1000
    eta = f['jets']['TauJets.eta'][main_mask]
    flav = f['jets']['HadronConeExclTruthLabelID'][main_mask]
    is_tau = (flav == 5)
    is_bkg = (flav == 0) 
    if key != "Deepset":
        pTau = f['jets'][f'{key}_pb'][main_mask]
        pBkg = f['jets'][f'{key}_pu'][main_mask]
        discs = disc_fct(np.row_stack([pTau, pBkg]))
        rej = calc_rej(discs[is_tau], discs[is_bkg], eff)
        result_dict = {
                'is_tau' : is_tau,
                'is_bkg' : is_bkg,
                'pt_bkg'  : pt[is_bkg],
                'eta_bkg' : eta[is_bkg],
                'pt_tau'  : pt[is_tau],
                'eta_tau' : eta[is_tau],
                'pTau' : pTau[is_tau],
                'pBkg' : pTau[is_bkg],
                'sig_eff' : eff,
                'disc_sig' : discs[is_tau],
                'disc_bkg' : discs[is_bkg],
                'disc' : discs,
                'rej' : rej
            }
        if prong == '0p':
            result_dict['rnn_wp'] = [0.50,0.65,0.90]
            result_dict['tight_rej'] = rej[np.argmin(np.abs(eff - 0.50))]
            result_dict['medium_rej'] = rej[np.argmin(np.abs(eff - 0.65))]
            result_dict['loose_rej'] = rej[np.argmin(np.abs(eff - 0.90))]
        if prong == '1p':
            result_dict['rnn_wp'] = [0.94,0.97,0.99]
            result_dict['tight_rej'] = rej[np.argmin(np.abs(eff - 0.94))]
            result_dict['medium_rej'] = rej[np.argmin(np.abs(eff - 0.97))]
            result_dict['loose_rej'] = rej[np.argmin(np.abs(eff - 0.99))]

        elif prong == 'mp':
            result_dict['rnn_wp'] = [0.80,0.91,0.98]
            result_dict['tight_rej'] = rej[np.argmin(np.abs(eff - 0.80))]
            result_dict['medium_rej'] = rej[np.argmin(np.abs(eff - 0.91))]
            result_dict['loose_rej'] = rej[np.argmin(np.abs(eff - 0.98))]
    
        return result_dict
    
    else:
        rnn_pTau = f['jets']['TauJets.RNNJetScore'][main_mask]
        rnn_pBkg = 1 - rnn_pTau
        rnn_discs = disc_fct(np.row_stack([rnn_pTau, rnn_pBkg]))
        rnn_rej = calc_rej(rnn_discs[is_tau], rnn_discs[is_bkg], eff)
        result_dict = {
                'is_tau' : is_tau,
                'is_bkg' : is_bkg,
                'pt_bkg'  : pt[is_bkg],
                'eta_bkg' : eta[is_bkg],
                'pt_tau'  : pt[is_tau],
                'eta_tau' : eta[is_tau],
                'pTau' : rnn_pTau[is_tau],
                'pBkg' : rnn_pBkg[is_bkg],
                'sig_eff' : eff,
                'disc_sig' : rnn_discs[is_tau],
                'disc_bkg' : rnn_discs[is_bkg],
                'disc' : rnn_discs,
                'rej' : rnn_rej
            }
        if prong == '0p':
            result_dict['rnn_wp'] = [0.50,0.65,0.90]
            result_dict['tight_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.50))]
            result_dict['medium_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.65))]
            result_dict['loose_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.90))]
        if prong == '1p':
            result_dict['rnn_wp'] = [0.94,0.97,0.99]
            result_dict['tight_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.94))]
            result_dict['medium_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.97))]
            result_dict['loose_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.99))]
        elif prong == 'mp':
            result_dict['rnn_wp'] = [0.80,0.91,0.98]
            result_dict['tight_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.80))]
            result_dict['medium_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.91))]
            result_dict['loose_rej'] = rnn_rej[np.argmin(np.abs(eff - 0.98))]

        return result_dict  

def plot_disc(results,reference_network,mode,desc,sample,model):
    n_sig_jets = np.sum(results[reference_network]['is_tau'])
    n_bkg_jets = np.sum(results[reference_network]['is_bkg'])
    plt.figure(figsize=(8,6))
    colors = ["#AA3377",
            "#228833",
            "#4477AA",
            "#CCBB44",
            "#EE6677",
            "#BBBBBB",]
    
    for i,network in enumerate(results.keys()):
        network_label = network.replace('LStt','SC1').replace('LS','SC0')
        if network == reference_network:
            plt.hist(results[network]['disc_sig'], bins=60, histtype='step', label=f'{network_label}_Tau', density=True, linestyle = '-', color = 'black', linewidth=1)
            plt.hist(results[network]['disc_bkg'], bins=60, histtype='step', label=f'{network_label}_Bkg', density=True, linestyle = ':', color = 'black', linewidth=1)

        else:
            plt.hist(results[network]['disc_sig'], bins=60, histtype='step', label=f'{network_label}_Tau', density=True, linestyle = '-', color = colors[i], linewidth=1)
            plt.hist(results[network]['disc_bkg'], bins=60, histtype='step', label=f'{network_label}_Bkg', density=True, linestyle = ':', color = colors[i], linewidth=1)
    
    ax = plt.gca()
    ymin,ymax = ax.get_ylim()
    plt.ylim(ymin,ymax*1.2)
    plt.xlabel(r'Discriminant : log $\left(\frac{p_{tau}}{p_{qcd}}\right)$',fontsize=15)
    # plt.xlabel(r'Discriminant : -log($1-p_{tau}$)',fontsize=15)
    # plt.xlabel(r'Discriminant : $p_{tau}$',fontsize=15)
    plt.ylabel('Normalized Counts',fontsize=15)
    ampl.draw_atlas_label(0.03,0.95,status='Simulation Internal',fontsize=13)
    plt.text(0.03, 0.91, f'{desc} \n $N_{{tau}} = {n_sig_jets:,}$, $N_{{qcd}} = {n_bkg_jets:,}$', transform=plt.gca().transAxes, fontsize=12, verticalalignment='top')
    plt.tight_layout()
    plt.xlim(-20,20)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.legend(loc='upper right', fontsize=9)
    os.makedirs(f'plots/{mode}-{model}', exist_ok=True)
    plt.savefig(f'plots/{mode}-{model}/{mode}-discriminant-{model}-{sample}.pdf',bbox_inches='tight')
    plt.show()
    plt.close()


def plot_roc(results,reference_network,mode,desc,sample,model):
    n_sig_jets = np.sum(results[reference_network]['is_tau'])
    n_bkg_jets = np.sum(results[reference_network]['is_bkg'])
    plot_roc = RocPlot(
        n_ratio_panels=1,
        ylabel="Background rejection",
        xlabel="$tau$-jet efficiency",
        atlas_second_tag=f'{desc} \n $N_{{tau}} = {n_sig_jets:,}$, $N_{{qcd}} = {n_bkg_jets:,}$',
        figsize=(7, 5),
        fontsize=12,
        )


    for i,network in enumerate(results.keys()):
        network_label = network.replace('LStt','SC1').replace('LS','SC0')
        plot_roc.draw_vlines(xs = results[network]['rnn_wp'], labels=['tight','medium','loose'],fontsize=9) if i==0 else None
        plot_roc.add_roc(
            Roc(
                results[network]['sig_eff'],
                results[network]['rej'],
                n_test=np.sum(results[network]['is_bkg']),
                rej_class="qcd",
                signal_class='$tau$-jets',
                label=network_label,
                colour='#000000' if network == f'Deepset' else None,
            ),
            reference=(reference_network == f'Deepset'),
        )
    plot_roc.set_ratio_class(1, "qcd")
    plot_roc.draw()
    plot_roc.savefig(f"plots/{mode}-{model}/{mode}-ROC-{model}-{sample}.pdf", bbox_inches='tight')


def plot_ptVsRej(results,reference_network,pT_range,mode,desc,sample,model):
    n_sig_jets = np.sum(results[reference_network]['is_tau'])
    n_bkg_jets = np.sum(results[reference_network]['is_bkg'])
    for id,wp in zip(['tight','medium','loose'],list(results.items())[0][1]['rnn_wp']):
        plot_bkg_rej = VarVsEffPlot( 
                        mode="bkg_rej",
                        ylabel="Light-flavour jets rejection",
                        xlabel=r"$p_{T}$ [GeV]",
                        logy=True,
                        logx=False,
                        atlas_second_tag=f'{desc} | eff = {wp*100}% \n $N_{{tau}} = {n_sig_jets:,}$, $N_{{qcd}} = {n_bkg_jets:,}$',
                        n_ratio_panels=1,
                        leg_fontsize = 7,
                        )
        for network in results.keys():
            network_label = network.replace('LStt','SC1').replace('LS','SC0')
            plot_bkg_rej.add(
                VarVsEff(x_var_sig= results[network]['pt_tau'],
                        disc_sig=results[network]['disc_sig'],
                        x_var_bkg=results[network]['pt_bkg'],
                        disc_bkg=results[network]['disc_bkg'],
                        bins=np.linspace(pT_range[0], pT_range[1], 30),
                        working_point=wp,
                        disc_cut=None,
                        flat_per_bin=False,
                        label=network_label,
                        colour='#000000' if network == f'Deepset' else None,
                        ),
                        reference = (reference_network == f'Deepset'),
                        )
        
        plot_bkg_rej.draw()
        plot_bkg_rej.savefig(f"plots/{mode}-{model}/{mode}-pTvsRej-{id}-{model}-{sample}.pdf", bbox_inches='tight')
        
def plot_rejection(results,reference_network,mode,desc,sample,model):
    rej_l = []
    rej_m = []
    rej_t = []
    x_ticks = []
    reference_l = results[reference_network]['loose_rej']
    reference_m = results[reference_network]['medium_rej']
    reference_t = results[reference_network]['tight_rej']
    for i,network in enumerate(results.keys()):
        if network == reference_network:
            continue       
        rej_l.append(results[network]['loose_rej']/reference_l)
        rej_m.append(results[network]['medium_rej']/reference_m)
        rej_t.append(results[network]['tight_rej']/reference_t)
        x_ticks.append(network.replace('LStt','SC1').replace('LS','SC0')[8:9])
    
    sorted_indices = np.argsort(x_ticks)
    x_ticks = np.array(x_ticks)[sorted_indices]
    rej_l = np.array(rej_l)[sorted_indices]
    rej_m = np.array(rej_m)[sorted_indices]
    rej_t = np.array(rej_t)[sorted_indices]

    plt.figure(figsize=(8,5))
    plt.plot(x_ticks,rej_l,marker='o',label='Loose',color='black')
    plt.plot(x_ticks,rej_m,marker='o',label='Medium',color='red')
    plt.plot(x_ticks,rej_t,marker='o',label='Tight',color='blue')
    plt.plot([0,len(x_ticks)-1],[1,1],linestyle='--',color='black',label=f'{reference_network}_rej',linewidth=0.5,alpha=0.5)
    plt.ylabel('Bkg Rejection w.r.t ref',fontsize=15)
    plt.xlabel('Sample Combination',fontsize=15)
    ampl.draw_atlas_label(0.03,0.95,status='Simulation Internal',fontsize=13)
    plt.text(0.03, 0.91, f'{desc}', transform=plt.gca().transAxes, fontsize=12, verticalalignment='top')
    plt.tight_layout()
    plt.xticks(x_ticks,fontsize=15)
    plt.yticks(fontsize=15)
    plt.xlim(-0.5,len(x_ticks)-0.5)
    y_min,y_max = plt.ylim()
    plt.ylim(y_min,y_max*1.1)
    plt.legend(loc='upper right', fontsize=9)
    os.makedirs(f'plots/{mode}-{model}', exist_ok=True)
    plt.savefig(f'plots/{mode}-{model}/{mode}-rejection-{model}-{sample}.pdf',bbox_inches='tight')
    plt.show()
    plt.close()
