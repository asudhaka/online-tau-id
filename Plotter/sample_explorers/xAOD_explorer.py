import ROOT

# Open the ROOT file
file = ROOT.TFile("/afs/cern.ch/user/a/asudhaka/eos_space/GN_tau/GNTau_samples/xAOD_samples/valid1.801166.Py8EG_A14NNPDF23LO_jj_JZ1.recon.AOD.e8514_e8528_s4159_s4114_r15271/AOD.36998839._003315.pool.root.1")
# file = ROOT.TFile("/afs/cern.ch/user/a/asudhaka/eos_space/GN_tau/GNTau_samples/mxaod_samples/user.ademaria.37313446._000001.output.root")
# file = ROOT.TFile("/afs/cern.ch/user/a/asudhaka/eos_space/GN_tau/GNTau_samples/THOR_samples/valid1.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8514_e8528_s4162_s4114_r15273_tid37003001_00/AOD.37003001._000001.pool.root.1")

# Get the CollectionTree
tree = file.Get("CollectionTree")

# Print information about TauJets in the CollectionTree
print("Information on branches containing 'TauJets':")
tree.Print("*")

# Access the auxiliary branch of TauJets

branch = tree.GetBranch("TauJetsAux.")

# Get the browsable content of the TauJetsAux. branch
browsables = branch.GetBrowsables()
print("\nBrowsable content of TauJetsAux. branch:")
browsables.Print()

# Close the file
file.Close()