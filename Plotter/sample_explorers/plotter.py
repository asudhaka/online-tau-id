import h5py
import multiprocessing as mp
import numpy as np
import os
import time
import yaml 
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt 
import tqdm
import argparse
import atlas_mpl_style as ampl

def get_parser():
    """
    Argument parser for Preprocessing script.

    Returns
    -------
    args: parse_args
    """
    parser = argparse.ArgumentParser(description="Converting command line options.")

    parser.add_argument(
        "-c",
        "--config_file",
        type=str,
        required=True,
        help="Enter the path of the config file to convert the sample.",
    )

    parser.add_argument(
        "-t",
        "--object_type",
        type=str,
        required=False,
        default='jets',
        help="Enter the name of the container to plot the variables",
    )

    parser.add_argument(
        "-p",
        "--nProngs",
        type=str,
        choices=['0', '1', '3', 'm', 'all'],
        required=True,
        help="Enter the number of prongs for the plots [0, 1, 3, m]",
    )
    
    parser.add_argument(
        "-m",
        "--mode",
        type=str,
        required=False,
        default='h5exp',
        help="Enter the mode for the script to run [h5exp, umami]"
    )

    return parser.parse_args()

def load_data(path,container,var):
  """
  Function to load data from a single file
  """
  with h5py.File(path, 'r') as f:
    return f[container][var][:]


def load_data_parallel(sample_paths, container, var):
  """
  Load data from multiple sample paths in parallel.
  Returns:
    list: A list of loaded data.
  """
  num_processes = min(mp.cpu_count(), len(sample_paths))
  start_time = time.time()

  pool = mp.Pool(num_processes)
  results = [pool.apply_async(load_data, args=(path, container, var)) for path in sample_paths]
  data = [result.get() for result in results]

  pool.close()
  pool.join()

  total_time = time.time() - start_time
  pbar.write(f"Loading {var} took: {total_time:.2f} seconds")

  return data

def perf_operation(array, operation_str):
  """
  This function takes an array and a mathematical operation string as input
  and attempts to evaluate the operation on the array elements.
  """
  try:
    result = eval(f'array{operation_str}')
    return result
  except (SyntaxError, NameError):
    print(operation_str, "is not a valid operation. Using the original array instead.")
    return array

def process_variable(var, sample_paths, container):
  """
  Process a variable by loading data in parallel and extracting signal and background variables.
  Returns:
    tuple: A tuple containing the signal and background values extracted from the data.
  """
  pbar.write(f'Processing {var}...')
  # Load data using multiprocessing
  data = load_data_parallel(sample_paths, container, var)

  SigVar = data[0][~np.isnan(data[0])]
  BkgVar = np.array(np.concatenate([data[i] for i in range(1, len(data))])).ravel()
  BkgVar = BkgVar[~np.isnan(BkgVar)]
  del data
  return SigVar, BkgVar

def plot_variable(var, SigVar, BkgVar, config, output_path):
  """
  Plots a variable for signal and background data
  Returns:
    None
  """
  
  if config['figure_properties']['atlas_tag']:
    ampl.use_atlas_style()

  font_size = config['figure_properties'].get('font_size', 12)

  if config['figure_properties']['ratio_plot']:
      fig = plt.figure(figsize=config['figure_properties']['figsize'], dpi=config['figure_properties']['dpi'])
      gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])  # 3:1 ratio for the main plot and the ratio plot
      gs.update(hspace=0.05) 
      ax_main = plt.subplot(gs[0])
      ax_ratio = plt.subplot(gs[1], sharex=ax_main)
      plt.subplots_adjust(hspace=0.05)

  else:
      fig, ax_main = plt.subplots(figsize=config['figure_properties']['figsize'], dpi=config['figure_properties']['dpi'])
      ax_ratio = None

  bkg_var_transformed = perf_operation(BkgVar, config['variables'][var]['operation']) if 'operation' in config['variables'][var] else BkgVar
  sig_var_transformed = perf_operation(SigVar, config['variables'][var]['operation']) if 'operation' in config['variables'][var] else SigVar

  bkg_hist = ax_main.hist(
      bkg_var_transformed,
      histtype=config['figure_properties']['histtype'] if 'histtype' in config['figure_properties'] else 'step',
      label='Background',
      bins=config['variables'][var]['bins'] if 'bins' in config['variables'][var] else None,
      range=config['variables'][var]['range'] if 'range' in config['variables'][var] and not config['figure_properties']['auto_range'] else None,
      density=config['figure_properties']['density'] or (config['variables'][var]['density'] if 'density' in config['variables'][var] else False),
      linewidth=2 if config['figure_properties']['atlas_tag'] else None,
      alpha=config['figure_properties']['alpha'] if 'alpha' in config['figure_properties'] else 1.0,
      color=config['figure_properties']['bkg_color'] if 'bkg_color' in config['figure_properties'] else 'blue'
  )

  sig_hist = ax_main.hist(
      sig_var_transformed,
      histtype=config['figure_properties']['histtype'] if 'histtype' in config['figure_properties'] else 'step',
      label='Signal',
      bins=config['variables'][var]['bins'] if 'bins' in config['variables'][var] else None,
      range=config['variables'][var]['range'] if 'range' in config['variables'][var] and not config['figure_properties']['auto_range'] else None,
      density=config['figure_properties']['density'] or (config['variables'][var]['density'] if 'density' in config['variables'][var] else False),
      linewidth=2 if config['figure_properties']['atlas_tag'] else None,
      alpha=config['figure_properties']['alpha'] if 'alpha' in config['figure_properties'] else 1.0,
      color=config['figure_properties']['sig_color'] if 'sig_color' in config['figure_properties'] else 'red'
  )

  ax_main.set_yscale(config['variables'][var]['yscale']) if not config['figure_properties']['density'] else None
  ax_main.set_ylabel(f'Number of {container}', fontsize=font_size)
  ymin, ymax = ax_main.get_ylim()
  ax_main.set_ylim(ymin, ymax * 1.15)
  ax_main.set_xlabel(config['variables'][var]['xlabel'] + f'-{prong} prong', fontsize=font_size)
  ax_main.set_xlim(config['variables'][var]['range']) if config['figure_properties']['atlas_tag'] and 'range' in config['variables'][var] else None
  ax_main.set_xticks(config['variables'][var]['bins']) if 'bins' in config['variables'][var] and type(config['variables'][var]['bins']) == list else None
  ax_main.tick_params(labelsize=font_size)

  if config['figure_properties']['atlas_tag']:
      ampl.plot.draw_atlas_label(ax=ax_main, x=0.02, y=0.95, status='int', simulation=config['figure_properties']['sim_tag'], fontsize=font_size, desc = config['figure_properties'].get('desc', f'{prong} Prong Jets'))
  ax_main.legend(loc=config['figure_properties']['legend_position'], fontsize=font_size)

  if config['figure_properties']['ratio_plot']:
      ax_main.set_xlabel('')
      # Compute the ratio
      bkg_hist_values, bkg_bin_edges = np.histogram(bkg_var_transformed, bins=config['variables'][var]['bins'], density=True if config['figure_properties']['density'] else False)
      sig_hist_values, sig_bin_edges = np.histogram(sig_var_transformed, bins=config['variables'][var]['bins'], density=True if config['figure_properties']['density'] else False)
      
      # To avoid division by zero, replace 0 values in bkg_hist_values with a small number
      bkg_hist_values = np.where(bkg_hist_values == 0, 1e-20, bkg_hist_values)
      ratio = sig_hist_values / bkg_hist_values

      # Compute bin centers
      bin_centers = 0.5 * (bkg_bin_edges[:-1] + bkg_bin_edges[1:])
      bin_width = bkg_bin_edges[1] - bkg_bin_edges[0]

      # ax_ratio.bar(bin_centers, ratio, width=bin_width, align='center', edgecolor='black', color='white')
      ax_ratio.step(bin_centers, ratio, where='post', color='black')
      # ax_ratio.plot(bin_centers, ratio, '-', color='black')
      
      # ax_ratio.hist(bin_centers, bins=bkg_bin_edges, weights=ratio, histtype='step', color='black')
      ax_ratio.set_ylabel('Sig/Bkg', fontsize=font_size)
      ax_ratio.set_xlabel(config['variables'][var]['xlabel'] + f'-{prong} prong', fontsize=font_size)
      ax_ratio.set_xticks(config['variables'][var]['bins']) if 'bins' in config['variables'][var] and type(config['variables'][var]['bins']) == list else None
      ax_ratio.axhline(1, color='gray', linestyle='--')
      ax_ratio.tick_params(axis='both', which='major', labelsize=font_size)
      ax_main.tick_params(axis='y', which='major', labelsize=font_size)
      ax_main.tick_params(axis='x', which='major', labelsize=0)
      # ax_ratio.set_ylim(0, 2)

  pdf_path = os.path.join(output_path, f'{var}-{prong}p.pdf')
  png_path = os.path.join(output_path, f'{var}-{prong}p.png')

  if not config['figure_properties']['ratio_plot']:
    plt.tight_layout() if config['figure_properties']['tight_layout'] else None

  if config['figure_properties']['save_pdf']:
    plt.savefig(pdf_path, bbox_inches='tight')
  if config['figure_properties']['save_png']:
    plt.savefig(png_path, bbox_inches='tight')

  plt.close()
  pbar.update(1)
  del SigVar, BkgVar

if __name__ == "__main__":

  args = get_parser()
  with open(args.config_file, 'r') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

  if config['only_plot']['flag']:
    variables = config['only_plot']['vars']
  else:
    variables = config['variables'].keys()
  
  if args.mode == 'h5exp':
    if args.nProngs == 'all':
      container = args.object_type
      output_dir = config['path']['output_dir']
      output_path = os.path.join(output_dir, container)
      os.makedirs(output_path, exist_ok=True)
      prongs = ['0','1','m','all']
      pbar = tqdm.tqdm(total=len(variables)*len(prongs))
      for prong in prongs:
        if prong == 'all':
            sig_path = [config['path']['input_dir_all'][0]]
            bkg_path = config['path']['input_dir_all'][1:]

        else:
            sig_dir = config['path']['input_dir_prong'] + f'/{prong}p/tau'
            bkg_dir = config['path']['input_dir_prong'] + f'/{prong}p/qcd'
            sig_path = [os.path.join(sig_dir, file) for file in os.listdir(sig_dir) if file.endswith(".h5")]
            bkg_path = [os.path.join(bkg_dir, file) for file in os.listdir(bkg_dir) if file.endswith(".h5")]

        sample_paths = sig_path + bkg_path
        assert (all(os.path.exists(path) for path in sample_paths))
        h5file = h5py.File(sample_paths[0], 'r')
        assert (container in h5file)
        
        for i, var in enumerate(variables):
          if var not in h5file[container].dtype.names:
              continue
          SigVar, BkgVar = process_variable(var, sample_paths, container)
          plot_variable(var, SigVar, BkgVar, config, output_path)
      pbar.close()

    else:
      container = args.object_type
      output_dir = config['path']['output_dir']
      output_path = os.path.join(output_dir, container)
      os.makedirs(output_path, exist_ok=True)

      prong = args.nProngs
      pbar = tqdm.tqdm(total=len(variables))
      sig_dir = config['path']['input_dir_prong'] + f'/{prong}p/tau'
      bkg_dir = config['path']['input_dir_prong'] + f'/{prong}p/qcd'
      sig_path = [os.path.join(sig_dir, file) for file in os.listdir(sig_dir) if file.endswith(".h5")]
      bkg_path = [os.path.join(bkg_dir, file) for file in os.listdir(bkg_dir) if file.endswith(".h5")]
      sample_paths = sig_path + bkg_path
      assert (all(os.path.exists(path) for path in sample_paths))

      h5file = h5py.File(sample_paths[0], 'r')
      assert (container in h5file)
      for i, var in enumerate(variables):
        if var not in h5file[container].dtype.names:
          continue
        SigVar, BkgVar = process_variable(var, sample_paths, container)
        plot_variable(var, SigVar, BkgVar, config, output_path)
      pbar.close()
      
  elif args.mode == 'umami':
    container = args.object_type
    output_dir = config['path']['output_dir']
    output_path = os.path.join(output_dir, 'umami', container)
    os.makedirs(output_path, exist_ok=True)
    prong = args.nProngs
    pbar = tqdm.tqdm(total=len(variables))
    sig_dir = config['path']['input_dir_umami']+ f'/{prong}p/components/train'
    bkg_dir = config['path']['input_dir_umami']+ f'/{prong}p/components/train'
    sig_path = [os.path.join(sig_dir, file) for file in os.listdir(sig_dir) if file.endswith("bjets.h5")]
    bkg_path = [os.path.join(bkg_dir, file) for file in os.listdir(bkg_dir) if file.endswith("ujets.h5")]
    sample_paths = sig_path + bkg_path
    assert (all(os.path.exists(path) for path in sample_paths))

    h5file = h5py.File(sample_paths[0], 'r')
    assert (container in h5file)
    for i, var in enumerate(variables):
      if var not in h5file[container].dtype.names:
        continue
      SigVar, BkgVar = process_variable(var, sample_paths, container)
      plot_variable(var, SigVar, BkgVar, config, output_path)
    pbar.close()