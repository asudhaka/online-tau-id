import h5py
import numpy as np
from tqdm import tqdm

def align_dtype(array, target_dtype):
    """
    Aligns the data types of a structured numpy array to the target dtype.
    If the dtype of a field differs, it is cast to the target dtype.
    """
    new_array = np.zeros(array.shape, dtype=target_dtype)
    for name in target_dtype.names:
        new_array[name] = array[name].astype(target_dtype[name])
    return new_array

def merge_and_shuffle_h5(signal_file, background_file, output_file, batch_size=1000):
    with h5py.File(signal_file, 'r') as sig_file, h5py.File(background_file, 'r') as bg_file:
        sig_jets = sig_file['jets']
        sig_tracks = sig_file['tracks']
        sig_cells = sig_file['cells']

        bg_jets = bg_file['jets']
        bg_tracks = bg_file['tracks']
        bg_cells = bg_file['cells']

        num_signal = sig_jets.shape[0]
        num_background = bg_jets.shape[0]
        total_samples = num_signal + num_background

        with h5py.File(output_file, 'w') as out_file:
            dset_jets = out_file.create_dataset('jets', shape=(total_samples,) + sig_jets.shape[1:], dtype=sig_jets.dtype, compression="gzip", chunks=True)
            dset_tracks = out_file.create_dataset('tracks', shape=(total_samples,) + sig_tracks.shape[1:], dtype=sig_tracks.dtype, compression="gzip", chunks=True)
            dset_cells = out_file.create_dataset('cells', shape=(total_samples,) + sig_cells.shape[1:], dtype=sig_cells.dtype, compression="gzip", chunks=True)

            write_index = 0

            for start in tqdm(range(0, max(num_signal, num_background), batch_size), desc="Processing Batches"):
                end = start + batch_size
                sig_end = min(end, num_signal)
                bg_end = min(end, num_background)

                sig_jets_batch = sig_jets[start:sig_end]
                sig_tracks_batch = sig_tracks[start:sig_end]
                sig_cells_batch = sig_cells[start:sig_end]


                bg_jets_batch = bg_jets[start:bg_end]
                bg_tracks_batch = bg_tracks[start:bg_end]
                bg_cells_batch = bg_cells[start:bg_end]



                if bg_jets_batch.shape[0] > 0:
                    bg_jets_batch = align_dtype(bg_jets_batch, sig_jets.dtype)
                    bg_tracks_batch = align_dtype(bg_tracks_batch, sig_tracks.dtype)
                    bg_cells_batch = align_dtype(bg_cells_batch, sig_cells.dtype)

                sig_jets_batch = sig_jets_batch[
                    [name for name in sig_jets_batch.dtype.names if name != 'flavour_label']
                ]
                bg_jets_batch = bg_jets_batch[
                    [name for name in bg_jets_batch.dtype.names if name != 'flavour_label']
                ]

                jets_batch = np.concatenate((sig_jets_batch, bg_jets_batch), axis=0)
                tracks_batch = np.concatenate((sig_tracks_batch, bg_tracks_batch), axis=0)
                cells_batch = np.concatenate((sig_cells_batch, bg_cells_batch), axis=0)

                batch_indices = np.arange(jets_batch.shape[0])
                np.random.shuffle(batch_indices)

                jets_batch = jets_batch[batch_indices]
                tracks_batch = tracks_batch[batch_indices]
                cells_batch = cells_batch[batch_indices]

                dset_jets[write_index:write_index + jets_batch.shape[0]] = jets_batch
                dset_tracks[write_index:write_index + tracks_batch.shape[0]] = tracks_batch
                dset_cells[write_index:write_index + cells_batch.shape[0]] = cells_batch

                write_index += jets_batch.shape[0]

    print(f"Data from {signal_file} and {background_file} merged and shuffled into {output_file} in batches.")

prong = 'm'
signal_file = f'/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/mp/ttbar(Sig).h5' 
background_file = f'/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/mp/dijet(Bg).h5'
output_file = f'/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/mp/ttbar(Sig)-dijet(Bg).h5'

print(f'Merging {signal_file} \n and \n {background_file} \n to {output_file} in batches...')
merge_and_shuffle_h5(signal_file, background_file, output_file, batch_size=50000)
