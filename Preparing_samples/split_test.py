import pandas as pd
import numpy as np
import h5py
import os, glob
import tqdm
import argparse
import os


def GetBatchesPerFile(filename: str, batch_size = 500000):
    """
    Split the file into batches to avoid that the loaded data is too large.

    Parameters
    ----------
    filename : str
        name of file to be split in batches

    Returns
    -------
    str
        filename
    list
        tuples of start and end index of batch

    """
    with h5py.File(filename, "r") as data_set:
        # get total number of jets in file
        total_n_jets = len(data_set["jets"])
        # first tuple is given by (0, batch_size)
        start_batch = 0
        end_batch = batch_size
        indices_batches = [(start_batch, end_batch)]
        # get remaining tuples of indices defining the batches
        while end_batch <= total_n_jets:
            start_batch += batch_size
            end_batch = start_batch + batch_size
            indices_batches.append((start_batch, end_batch))
    return (filename, indices_batches)

def building_new(var, label, first_array):
    new_array = np.zeros(
        first_array.shape, 
        dtype=(first_array.dtype.descr + [(label, '<f4')]))
    existing_keys = list(first_array.dtype.fields.keys())
    new_array[existing_keys] = first_array[existing_keys]
    new_array[label] = var
    return new_array

def jets_generator(files_in_batches: list, tracks_name = 'tracks', cells_name = 'cells'):
    """
    Helper function to extract jet and track information from a h5 ntuple.

    Parameters
    ----------
    files_in_batches : list
    tuples of filename and tuple of start and end index of batch

    Yields
    -------
    numpy.ndarray
    jets
    numpy.ndarray
    tracks
    numpy.ndarray
    cells
    """
    for filename, batches in files_in_batches:
        with h5py.File(filename, "r") as data_set:
            for batch in batches:
                jets = data_set["jets"][batch[0] : batch[1]]
                tracks = data_set[tracks_name][batch[0] : batch[1]]
                cells = data_set[cells_name][batch[0] : batch[1]]
                yield (jets, tracks, cells)


def split_dataset(input_files, 
                  output_file,
                  sample, # Signal or Background
                  tracks_name='tracks', 
                  cells_name = 'cells', 
                  n_jets_to_get = 50000, 
                  n_jets_per_file=int(1e6),
                  processes=[6]):
    create_file = True
    jets_curr_file = 0
    _output_file = output_file
    displayed_writing_output = True
    files_in_batches = map(GetBatchesPerFile, input_files)
    pbar = tqdm.tqdm(total=n_jets_to_get)
    output_file = f'{_output_file[:-3]}_{n_jets_to_get // n_jets_per_file}.h5'
    print(f'Writing out {processes} test jets')
    for jets, tracks, cells in jets_generator(files_in_batches):
        if len(jets) == 0:
            continue
        jets_in_this_file = jets.size
        mask = (jets['TauJets.mcEventNumber']%9 == 8)
        process_mask = np.zeros(jets.size, dtype=bool)
        for process in processes:
            process_mask |= (jets['TauJets.Sample'] == process)
        mask &= process_mask 
        if sample == 'Signal':
            mask &= (jets['HadronConeExclTruthLabelID'] == 5)
        elif sample == 'Background':
            mask &= (jets['HadronConeExclTruthLabelID'] == 0)
        elif sample == 'all':   
            mask &= (jets['HadronConeExclTruthLabelID'] == 5) | (jets['HadronConeExclTruthLabelID'] == 0)
            

        jets = jets[mask]
        tracks = tracks[mask]
        cells = cells[mask]
        
        pbar.update(jets_in_this_file)
        n_jets_to_get -= jets.size
        if jets_curr_file >= n_jets_per_file:
             create_file = True
             jets_curr_file = 0
             output_file = f'{_output_file[:-3]}_{n_jets_to_get // n_jets_per_file}.h5'
        else:
             jets_curr_file += jets.size
        if create_file:
            pbar.write("Creating output file: " + output_file)
            create_file = False  # pylint: disable=W0201:
            # write to file by creating dataset
            with h5py.File(output_file, "w") as out_file:
                out_file.create_dataset(
                    "jets",
                    data=jets,
                    compression="gzip",
                    chunks=True,
                    maxshape=(None,),
                )
                out_file.create_dataset(
                    tracks_name,
                    data=tracks,
                    compression="gzip",
                    chunks=True,
                    maxshape=(None, tracks.shape[1]),
                )
                out_file.create_dataset(
                    cells_name,
                    data=cells,
                    compression="gzip",
                    chunks=True,
                    maxshape=(None, cells.shape[1]),
                )
        else:
            # appending to existing dataset
            if displayed_writing_output:
                pbar.write("Writing to output file: " + output_file)
            with h5py.File(output_file, "a") as out_file:
                out_file["jets"].resize(
                    (out_file["jets"].shape[0] + jets.shape[0]),
                    axis=0,
                )
                out_file["jets"][-jets.shape[0] :] = jets
                out_file[tracks_name].resize(
                    (
                        out_file[tracks_name].shape[0]
                        + tracks.shape[0]
                    ),
                    axis=0,
                )
                out_file[tracks_name][
                    -tracks.shape[0] :
                ] = tracks
                out_file[cells_name].resize(
                    (
                        out_file[cells_name].shape[0]
                        + cells.shape[0]
                    ),
                    axis=0,
                )
                out_file[cells_name][
                    -cells.shape[0] :
                ] = cells
            displayed_writing_output = False
        if n_jets_to_get <= 0:
            break
    pbar.close()
    



prongs = '0p'
sample = 'Background' # 'Background'
process = 'dijet' # 'gammatautau', 'dijet', 'ttbar_semilep', 'Ztautau'

gammatautau_dataset = [f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/gammatautau/gammatautau_samp0_1.h5']
dijet_dataset = [f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/dijet/dijet_samp0_1.h5',
                 f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/dijet/dijet_samp1_1.h5',
                 f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/dijet/dijet_samp2_1.h5',
                 f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/dijet/dijet_samp3_1.h5',
                 f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/dijet/dijet_samp4_1.h5',
                 f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/dijet/dijet_samp5_1.h5',
                 f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/dijet/dijet_samp6_1.h5',]
ttbar_semilep_dataset = [f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/ttbar_semilep/ttbar_semilep_samp0_1.h5']
Ztautau_dataset = [f'/home/aponnu/Desktop/ColdStorage/labelled_datasets/{prongs}/{sample}/Ztautau/Ztautau_samp0_1.h5']

datasets = {
    'gammatautau' : gammatautau_dataset,
    'dijet' : dijet_dataset,
    'ttbar_semilep' : ttbar_semilep_dataset,    
    'Ztautau' : Ztautau_dataset,
}

outpath = f'/home/aponnu/Desktop/AthulFiles/upp_LegSampTest/{prongs}/'
process_key = {
    'gammatautau':[0],
    'dijet':[1,2,3,4],
    'ttbar_semilep':[5],
    'Ztautau':[6],
    }

for samp, samp_files in datasets.items():
    if samp != process:
        continue
    for i, samp_file in enumerate(samp_files):
        # get total number of jets in file
        with h5py.File( samp_file, "r") as data_set:
            total_n_jets = len(data_set["jets"])
        print(f'DATASET: {samp_file} with {total_n_jets} jets')
        split_dataset([samp_file], 
            f'{outpath}/{process}(Sig)_{i}.h5' if sample == 'Signal' else f'{outpath}/{process}(Bkg)_{i}.h5',
            n_jets_to_get=total_n_jets, 
            n_jets_per_file=10_000_000,
            sample=sample,
            processes = process_key[process])