import h5py, yaml, os, argparse, tqdm
import numpy as np

def get_parser():
    parser = argparse.ArgumentParser(description='Reweight samples')


    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="path for the file with njets",
    )

    return parser.parse_args()

def GetBatchesPerFile(filename: str, batch_size):
    """
    Split the file into batches to avoid that the loaded data is too large.

    Parameters
    ----------
    filename : str
        name of file to be split in batches

    Returns
    -------
    str
        filename
    list
        tuples of start and end index of batch

    """
    with h5py.File(filename, "r") as data_set:
        # get total number of jets in file
        total_n_jets = len(data_set["jets"])
        # first tuple is given by (0, batch_size)
        start_batch = 0
        end_batch = batch_size
        indices_batches = [(start_batch, end_batch)]
        # get remaining tuples of indices defining the batches
        while end_batch <= total_n_jets:
            start_batch += batch_size
            end_batch = start_batch + batch_size
            indices_batches.append((start_batch, end_batch))
    return (filename, indices_batches)

def building_new(var, label, first_array):
    new_array = np.zeros(
        first_array.shape, 
        dtype=(first_array.dtype.descr + [(label, '<f4')]))
    existing_keys = list(first_array.dtype.fields.keys())
    new_array[existing_keys] = first_array[existing_keys]
    new_array[label] = var
    return new_array

def jets_generator(files_in_batches: list):
    """
    Helper function to extract jet and track information from a h5 ntuple.

    Parameters
    ----------
    files_in_batches : list
    tuples of filename and tuple of start and end index of batch

    Yields
    -------
    numpy.ndarray
    jets
    numpy.ndarray
    tracks
    numpy.ndarray
    cells
    """
    for filename, batches in files_in_batches:
        with h5py.File(filename, "r") as data_set:
            for batch in batches:
                # load jets in batches
                jets = data_set["jets"][batch[0] : batch[1]]
                tracks = data_set['tracks'][batch[0] : batch[1]]
                cells = data_set['cells'][batch[0] : batch[1]]
                yield (jets, tracks, cells)


def prep_samples(input_files, output_file, n_jets_to_get, n_jets_per_file,n_file=0):
    create_file = True
    jets_curr_file = 0
    _output_file = output_file
    displayed_writing_output = True
    batch_size = min(n_jets_to_get, 50000)
    files_in_batches = map(GetBatchesPerFile, input_files, [batch_size] * len(input_files))
    pbar = tqdm.tqdm(total=n_jets_to_get)
    output_file = f'{_output_file[:-3]}_{n_file}.h5'
    for jets, tracks, cells in jets_generator(files_in_batches):
        if len(jets) == 0:
            continue
        jets_in_this_file = jets.size            
        pbar.update(jets_in_this_file)
        n_jets_to_get -= jets.size
        if jets_curr_file >= n_jets_per_file:
             create_file = True
             jets_curr_file = 0
             output_file = f'{_output_file[:-3]}_{n_file}.h5'
        else:
             jets_curr_file += jets.size
             
        if create_file:
            pbar.write("Creating output file: " + output_file)
            create_file = False  # pylint: disable=W0201:
            # write to file by creating dataset
            with h5py.File(output_file, "w") as out_file:
                out_file.create_dataset(
                    "jets",
                    data=jets,
                    compression="gzip",
                    chunks=True,
                    maxshape=(None,),
                )
                out_file.create_dataset(
                    'tracks',
                    data=tracks,
                    compression="gzip",
                    chunks=True,
                    maxshape=(None, tracks.shape[1]),
                )
                out_file.create_dataset(
                    'cells',
                    data=cells,
                    compression="gzip",
                    chunks=True,
                    maxshape=(None, cells.shape[1]),
                )
        else:
            # appending to existing dataset
            if displayed_writing_output:
                pbar.write("Writing to output file: " + output_file)
            with h5py.File(output_file, "a") as out_file:
                out_file["jets"].resize(
                    (out_file["jets"].shape[0] + jets.shape[0]),
                    axis=0,
                )
                out_file["jets"][-jets.shape[0] :] = jets
                out_file['tracks'].resize(
                    (
                        out_file['tracks'].shape[0]
                        + tracks.shape[0]
                    ),
                    axis=0,
                )
                out_file['tracks'][
                    -tracks.shape[0] :
                ] = tracks
                out_file['cells'].resize(
                    (
                        out_file['cells'].shape[0]
                        + cells.shape[0]
                    ),
                    axis=0,
                )
                out_file['cells'][
                    -cells.shape[0] :
                ] = cells
            displayed_writing_output = False
        if n_jets_to_get <= 0:
            break
    pbar.close()



data_folder = '/home/aponnu/Desktop/ColdStorage/labelled_datasets'
args = get_parser()
with open(args.config, 'r') as file:
    config = yaml.load(file, Loader=yaml.FullLoader)
samples = ['gammatautau', 'dijet', 'ttbar_semilep', 'Ztautau']
version = args.config.split('/')[-1][:-5]
path = f'/home/aponnu/Desktop/ColdStorage/labelled_datasets_{version}/' 

# Create directories if it doenst exist
for prong in ['0p','1p','mp']:
    for split in ['Signal', 'Background']:
        for sample in samples:
            if not os.path.exists(path+f'{prong}/{split}/{sample}/'):
                os.makedirs(path+f'{prong}/{split}/{sample}/')

# Copy config file to new directory
os.system(f'cp {args.config} {path}')

for prong in ['0p','1p','mp']:
    for split in ['Signal', 'Background']:
        for sample in samples:
            if sample not in config[prong]:
                continue
            jets_required = config[prong][sample][f'N_{split}_train'] + (config[prong][sample][f'N_{split}_test'] *2)

            if jets_required == 0:
                continue
            h5_list = os.listdir(f'{data_folder}/{prong}/{split}/{sample}/')
            h5_paths = [f'{data_folder}/{prong}/{split}/{sample}/{h5}' for h5 in h5_list if 'vds' not in h5]
            jets_rem = jets_required
            print(f'{prong}/{split}/{sample} : {jets_required} jets required')
            for i,h5_path in enumerate(h5_paths):
                jet_count = h5py.File(h5_path, 'r')['jets'].shape[0]
                process_jets = min(jet_count, jets_rem)
                prep_samples([h5_path], f'{path}{prong}/{split}/{sample}/{sample}.h5', process_jets,process_jets,n_file=i)
                jets_rem -= process_jets
                if jets_rem <= 0:
                    break
            print(f'Processed {jets_required} jets from {prong}/{split}/{sample}')